function createCard(name, description, pictureUrl, location, starts, ends) {
    return `
    <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <p class="card-subtitle mb-2 text-muted">${location}</p>
        <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${starts}-${ends}</div>
    </div>

    `;
}

window.addEventListener('DOMContentLoaded', async () => {

        const url = 'http://localhost:8000/api/conferences/';
        try {
            const response = await fetch(url);
            if (!response.ok) {
            // Figure out what to do when the response is bad
            } else {
            const data = await response.json();
            let counter = 0
            for (let conference of data.conferences) {
                counter += 1
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const location = details.conference.location["name"];
                const title = details.conference.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const n_starts = new Date(details.conference.starts);
                const n_ends = new Date(details.conference.ends);
                const starts = n_starts.toLocaleDateString();
                const ends = n_ends.toLocaleDateString();
                const html = createCard(title, description, pictureUrl, location, starts, ends);
                const column1 = document.querySelector('.col1');
                const column2 = document.querySelector('.col2');
                const column3 = document.querySelector('.col3');
                if (counter%3==1){
                column1.innerHTML += html;
                } else if (counter%3==2){
                column2.innerHTML += html;
                } else {
                column3.innerHTML += html;
            }
            }
            }
        }
        } catch (e) {
          // Figure out what to do if an error is raised
        }

    });
